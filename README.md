# Spring Boot Utils

[![pipeline status](https://gitlab.com/tincore/spring-boot/badges/master/pipeline.svg)](https://gitlab.com/tincore/spring-boot/-/commits/master)

## Spring Boot Starter MQTT

Spring boot starter for MQTT.

### Installation

Add maven dependency to activate

```
    <dependency>
        <groupId>com.tincore</groupId>
        <artifactId>spring-boot-builder</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
```

### Configuration

Add mqtt property in your yaml/properties file.

```
mqtt:
    enabled: true
    autostart: true
    autostart-delay: PT1S
    clients:
    - id: clientIdentification
      uris: tcp://127.0.0.1:1883 # Server Uri. Default value tcp://127.0.0.1:1883

# Alternatively you may declare multiple uris
#      uris: 
#        - tcp://127.0.0.1:1883
#        - tcp://otherserver:1883

      username: username # Server user name (Optional)
      password: password # Server password (Optional)
      persistenceDirectory: ~/somePath # Path to message persistence. If not set will use memory (Optional)
      enableSharedSubscription: true # Allow shared MQTT subscriptions (Optional)
      publishQos: AT_MOST_ONCE # One of AT_MOST_ONCE, AT_LEAST_ONCE, EXACTLY_ONCE
      subscribeQos: AT_MOST_ONCE # One of AT_MOST_ONCE, AT_LEAST_ONCE, EXACTLY_ONCE
      maxReconnectDelay: PT60S # Maximum time to wait between reconnects
      keepAliveInterval: PT60S
      connectionTimeout: PT30S
      executorServiceTimeout: PT10S
      cleanSession: true
      automaticReconnect: true
      lwt: # Last will testament (optional)
        topic: someLastWillTopic
        payload: some payload
        qos: AT_MOST_ONCE # One of AT_MOST_ONCE, AT_LEAST_ONCE, EXACTLY_ONCE
        retained: true # Mqtt retained        

    - id: otherClientIdentification
    ...
    
```

You can customize different aspects by providing your own implementation of the following beans:

```
    MqttAsyncClientFactory.class
    MqttConnectOptionsFactory.class
    MqttPublisher.class
    MqttPayloadReadConverterFactory.class
    MqttPayloadWriteConverter.class
```

### Usage

#### Subscribe MQTT

##### Annotations

Subscribe on topic and get payload as string

```
        @MqttMapping("someTopic")
        public void onMessage(String payload) {
```

Subscribe on topic and get payload as byte array

```
        @MqttMapping("someTopic")
        public void onMessage(byte[] payload) {
```

Subscribe on topic filter and get topic and payload as string

```
        @MqttMapping("someTopic/+")
        public void onMessage(String topic, String payload) {
```

Subscribe and convert payload to json or other supported type. Uses registered converter.

```
        @MqttMapping("someTopic/+")
        public void onMessage(String topic, MyPojo payload) {

        @MqttMapping("someTopic/+")
        public void onMessage(String topic, UUID payload) {
```

Subscribe multiple mappings on same method

```
        @MqttMapping("someTopic/+")
        @MqttMapping("someOtherTopic/sub1/sub2")
        public void onMessage(String topic, MyPojo payload) {
```

Subscribe payload with different parameter name

```
        @MqttMapping("someTopic/+")
        public void onMessage(String topic, @MqttPayload MyPojo myPojo) {
```

Subscribe topic with different parameter name

```
        @MqttMapping("someTopic/+")
        public void onMessage(@MqttTopic String cause, MyPojo myPojo) {
```

Subscribe topic with filter and parameters

```
        @MqttMapping("someTopic/{parameter1}/sub/{parameter2}")
        public void onMessage(String parameter1, String parameter2, String payload) {
```

Subscribe topic with filter and parameters

```
        @MqttMapping("someTopic/{parameter1}/sub/{parameter2}")
        public void onMessage(String parameter1, String parameter2, String payload) {
```

Subscribe topic with filter and parameter with different name

```
        @MqttMapping("someTopic/{para1}/sub/#")
        public void onMessage(@MqttPathVariable("param1") String subTopic, String payload) {
```

Subscribe topic and bind MqttMessage

```
        @MqttMapping("someTopic/{para1}/sub/#")
        public void onMessage(@MqttPathVariable("param1") String subTopic, MqttMessage message) {
```

##### Programmatically

Inject reference to MqttSubscriberRegistry and MqttClientManager

```
    MqttSubscriberRegistry mqttSubscriberRegistry;
    MqttClientManager mqttClientManager;
```

Add subscribers to registry

```
        TestMqttReceivedMessageHandler mqttMessageHandler = new TestMqttReceivedMessageHandler();
       
        MqttSubscriber subscriber = new MqttSubscriber(List.of(new MqttSubscriberMapping(TOPIC_STRING_A)), (topic, mqttMessage, mqttSubscriberMapping, clientId) -> {
            // ....
        })
        
        mqttSubscriberRegistry.addMqttSubscriber(subscriber);
```

Send subscription updates to server

```
        mqttClientManager.sendSubscriptions();
```

#### Publish MQTT

Inject reference to MqttPublisher

```
    MqttPublisher mqttPublisher;
```

### Other topics

#### Maven deploy to artifactory

Use artifactory profile (by default deploys to gitlab)

```
mvn deploy -Partifactory -Dartifactory.password=*****
```

