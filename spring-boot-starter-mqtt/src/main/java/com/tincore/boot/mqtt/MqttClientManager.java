package com.tincore.boot.mqtt;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.*;
import org.springframework.beans.factory.DisposableBean;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Slf4j
public class MqttClientManager implements DisposableBean {

    private final MqttSubscriberRegistry mqttSubscriberRegistry;

    private final Map<String, IMqttAsyncClient> clientsByClientId = new HashMap<>();
    private final Map<String, MqttProperties.Client> clientPropertiesByClientId = new HashMap<>();
    private final Map<String, MqttConnectOptions> mqttConnectOptionsByClientId = new HashMap<>();
    private final Map<String, ClientSubscriptionProject> clientSubscriptionProjectsByClientId = new HashMap<>();

    private final ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(0);

    private String defaultClientId;

    public MqttClientManager(MqttProperties mqttProperties, MqttAsyncClientFactory mqttAsyncClientFactory, MqttConnectOptionsFactory mqttConnectOptionsFactory, MqttSubscriberRegistry mqttSubscriberRegistry) {
        this.mqttSubscriberRegistry = mqttSubscriberRegistry;

        if (!mqttProperties.isEnabled()) {
            return;
        }

        if (mqttProperties.getClients().isEmpty()) {
            throw new IllegalStateException("MqttProperties expects at least one client declared (mqtt.client:) but it is empty.");
        }

        mqttProperties
                .getClients()
                .forEach(
                        p -> {
                            String clientId = p.getId();
                            try {
                                MqttConnectOptions mqttConnectOptions = mqttConnectOptionsFactory.createMqttConnectOptions(p);
                                IMqttAsyncClient client = mqttAsyncClientFactory.createMqttAsyncClient(clientId, p.getPersistenceDirectory(), mqttConnectOptions.getServerURIs());
                                if (client != null) {
                                    if (defaultClientId == null) {
                                        defaultClientId = clientId;
                                        log.info("Default mqtt clientId={}", defaultClientId);
                                    }
                                    clientPropertiesByClientId.put(clientId, p);
                                    clientsByClientId.put(clientId, client);
                                    mqttConnectOptionsByClientId.put(clientId, mqttConnectOptions);

                                    client.setCallback(
                                            new MqttCallbackExtended() {
                                                private final String clientId = client.getClientId();

                                                @Override
                                                public void connectComplete(boolean reconnect, String serverURI) {
                                                    log.info("Connect completed. clientId={}, serverUri={}, isReconnect={}", clientId, serverURI, reconnect);
                                                    sendSubscriptions(client, true);
                                                }

                                                @Override
                                                public void connectionLost(Throwable cause) {
                                                    log.warn("Server connection lost. clientId={}", clientId);
                                                }

                                                @Override
                                                public void deliveryComplete(IMqttDeliveryToken token) {
                                                    log.trace("Server delivery completed. clientId={}", clientId);
                                                }

                                                @Override
                                                public void messageArrived(String topic, MqttMessage message) {
                                                    log.trace("Server message arrived. clientId={}, topic={}", clientId, topic);
                                                    for (MqttSubscriber subscriber : mqttSubscriberRegistry.getMqttSubscribers()) {
                                                        subscriber.getMqttSubscriberMappingByTopicMatching(clientId, topic).ifPresent(d -> subscriber.getMessageHandler().onMessage(topic, message, d, clientId));
                                                    }
                                                }
                                            });
                                }
                            } catch (MqttException e) {
                                throw new IllegalStateException(e);
                            }
                        });
    }

    private void close(IMqttAsyncClient client) {
        synchronized (client) {
            try {
                client.close();
            } catch (MqttException e) {
                log.error("Mqtt disconnect failed. clientId={}", client.getClientId(), e);
            }
        }
    }

    private void closeAll() {
        clientsByClientId.values().forEach(this::close);
    }

    private void connect(IMqttAsyncClient client, MqttConnectOptions mqttConnectOptions) {
        synchronized (client) {
            try {
                if (client.isConnected()) {
                    log.debug("Already connected. Skipping. clientId={}", client.getClientId());
                    return;
                }
                log.info("Connecting. clientId={}", client.getClientId());
                client.connect(
                        mqttConnectOptions,
                        null,
                        new IMqttActionListener() {
                            @Override
                            public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                                log.error("Connect failed. clientId={}. Scheduling reconnect in {}ms", client.getClientId(), mqttConnectOptions.getMaxReconnectDelay(), exception);
                                scheduleConnect(client, mqttConnectOptions, mqttConnectOptions.getMaxReconnectDelay());
                            }

                            @Override
                            public void onSuccess(IMqttToken asyncActionToken) {
                                log.info("Connect success. Subscribing. clientId={}", client.getClientId());
                            }
                        });
            } catch (MqttException e) {
                log.error("Connect error clientId=" + client.getClientId(), e);
            }
        }
    }

    public void connectAll(Duration delay) {
        clientsByClientId.values().forEach(c -> scheduleConnect(c, mqttConnectOptionsByClientId.get(c.getClientId()), delay.toMillis()));
    }

    public ClientSubscriptionProject createClientSubscriptionProject(String clientId) {
        return createClientSubscriptionProject(clientId, clientPropertiesByClientId.get(clientId), mqttSubscriberRegistry.getMqttSubscribers());
    }

    public ClientSubscriptionProject createClientSubscriptionProject(String clientId, MqttProperties.Client clientProperties, List<MqttSubscriber> mqttSubscribers) {

        boolean enableSharedSubscription = clientProperties.isEnableSharedSubscription();
        MqttQoS subscribeQos = clientProperties.getSubscribeQos();

        Set<MqttSubscriberMapping> exclusiveMqttSubscriberMappingsAccumulator = new HashSet<>();
        mqttSubscribers.stream()
                .filter(s -> s.isSubscriberByClientId(clientId))
                .flatMap(s -> s.getMqttSubscriberMappings().stream())
                .forEach(
                        m -> {
                            String candidateTopicFilter = m.getTopicFilter(enableSharedSubscription);
                            String candidateTopicFilterAsTopicSample = getTopicFilterAsTopicSample(candidateTopicFilter);
                            MqttQoS candidateQos = m.getQos(subscribeQos);

                            boolean moreGeneralThanCandidateTopicFilterFound =
                                    exclusiveMqttSubscriberMappingsAccumulator.stream()
                                            .anyMatch(ud -> ud.getQos(subscribeQos) == candidateQos && MqttTopic.isMatched(ud.getTopicFilter(enableSharedSubscription), candidateTopicFilterAsTopicSample));

                            if (!moreGeneralThanCandidateTopicFilterFound) {
                                List<MqttSubscriberMapping> lessGeneralExistingTopicFilters =
                                        exclusiveMqttSubscriberMappingsAccumulator.stream()
                                                .filter(ud -> ud.getQos(subscribeQos) == candidateQos)
                                                .filter(
                                                        ud -> {
                                                            String uniqueTopicFilter = ud.getTopicFilter(enableSharedSubscription);
                                                            String uniqueTopicFilterAsTopicSample = getTopicFilterAsTopicSample(uniqueTopicFilter);
                                                            return MqttTopic.isMatched(candidateTopicFilter, uniqueTopicFilterAsTopicSample);
                                                        })
                                                .collect(Collectors.toList());
                                lessGeneralExistingTopicFilters.forEach(exclusiveMqttSubscriberMappingsAccumulator::remove);
                                // Add this one
                                exclusiveMqttSubscriberMappingsAccumulator.add(m);
                            }
                        });

        if (exclusiveMqttSubscriberMappingsAccumulator.isEmpty()) {
            log.warn("Subscription empty. clientId={}", clientId);
            return ClientSubscriptionProject.EMPTY;
        }

        return new ClientSubscriptionProject(exclusiveMqttSubscriberMappingsAccumulator.stream().map(m -> new ClientSubscriptionProjectEntry(m.getTopicFilter(enableSharedSubscription), m.getQos(subscribeQos))).collect(Collectors.toList()));
    }

    @Override
    public void destroy() {
        log.info("Disconnecting and closing mqtt clients.");
        disconnectAll();
        closeAll();
        clientsByClientId.clear();
    }

    private void disconnect(IMqttAsyncClient client) {
        synchronized (client) {
            try {
                if (client.isConnected()) {
                    client.disconnect();
                }
            } catch (MqttException e) {
                log.error("Mqtt disconnect failed. clientId={}", client.getClientId(), e);
            }
        }
    }

    public void disconnectAll() {
        clientsByClientId.values().forEach(this::disconnect);
    }

    public IMqttAsyncClient getClientByClientId(String clientId) {
        return clientsByClientId.get(clientId);
    }

    public MqttProperties.Client getClientPropertiesByClientId(String clientId) {
        return clientPropertiesByClientId.get(clientId);
    }

    public String getDefaultClientId() {
        return defaultClientId;
    }

    private String getTopicFilterAsTopicSample(String candidateTopicFilter) {
        return candidateTopicFilter.replace('+', '\u0000').replace("#", "\u0000/\u0000");
    }

    public Boolean isClientConnected(String clientId) {
        return getClientByClientId(clientId).isConnected();
    }

    private void scheduleConnect(IMqttAsyncClient client, MqttConnectOptions mqttConnectOptions, long delayMillis) {
        scheduledExecutorService.schedule(() -> connect(client, mqttConnectOptions), delayMillis, TimeUnit.MILLISECONDS);
    }

    /** Reapply subscription changes */
    public void sendSubscriptions() {
        log.debug("Sending updated subscriptions to all clients");
        // Possible race condition here? Investigate
        clientsByClientId.values().stream().filter(IMqttAsyncClient::isConnected).forEach(c -> sendSubscriptions(c, false));
    }

    /**
     * Reapply subscription changes
     *
     * @param client
     */
    public void sendSubscriptions(IMqttAsyncClient client) {
        sendSubscriptions(client, false);
    }

    private void sendSubscriptions(IMqttAsyncClient client, boolean forceSend) {
        synchronized (client) {
            String clientId = client.getClientId();
            try {
                ClientSubscriptionProject currentClientSubscriptionProject = forceSend ? ClientSubscriptionProject.EMPTY : clientSubscriptionProjectsByClientId.get(clientId);
                ClientSubscriptionProject newClientSubscriptionProject = createClientSubscriptionProject(clientId);

                List<ClientSubscriptionProjectEntry> toCreateEntries = newClientSubscriptionProject.getEntries().stream().filter(e -> currentClientSubscriptionProject.getEntries().stream().noneMatch(e::equals)).collect(Collectors.toList());
                List<ClientSubscriptionProjectEntry> toRemoveEntries = currentClientSubscriptionProject.getEntries().stream().filter(e -> newClientSubscriptionProject.getEntries().stream().noneMatch(e::equals)).collect(Collectors.toList());

                if (!toRemoveEntries.isEmpty()) {
                    String[] topicFilters = toRemoveEntries.stream().map(ClientSubscriptionProjectEntry::getTopicFilter).toArray(String[]::new);
                    if (log.isInfoEnabled()) {
                        log.info("Unsubscribing topics. clientId={}, topicFilters={}", clientId, Arrays.toString(topicFilters));
                    }
                    client.unsubscribe(topicFilters);
                }
                if (!toCreateEntries.isEmpty()) {
                    String[] topicFilters = toCreateEntries.stream().map(ClientSubscriptionProjectEntry::getTopicFilter).toArray(String[]::new);
                    int[] qOSs = toCreateEntries.stream().mapToInt(e -> e.getQOS().getValue()).toArray();
                    if (log.isInfoEnabled()) {
                        log.info("Subscribing topics. clientId={}, topicFilters={}, qOSs={}", clientId, Arrays.toString(topicFilters), Arrays.toString(qOSs));
                    }

                    client.subscribe(topicFilters, qOSs);

                    log.info("Subscribed to topics. clientId={}", clientId);
                }
                clientSubscriptionProjectsByClientId.put(clientId, newClientSubscriptionProject);

            } catch (MqttException e) {
                log.error("Subscribe error, clientId={}", clientId, e);
                clientSubscriptionProjectsByClientId.remove(clientId);
            }
        }
    }

    @Data
    public static class ClientSubscriptionProject {
        public static final ClientSubscriptionProject EMPTY = new ClientSubscriptionProject(Collections.emptyList());

        private final List<ClientSubscriptionProjectEntry> entries;
    }

    @Data
    public static class ClientSubscriptionProjectEntry {
        private final String topicFilter;
        private final MqttQoS qOS;
    }
}
