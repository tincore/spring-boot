package com.tincore.boot.mqtt;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.util.StringUtils;

import java.nio.charset.StandardCharsets;

@FunctionalInterface
public interface MqttConnectOptionsFactory {

    MqttConnectOptions createMqttConnectOptions(MqttProperties.Client clientProperties);

    default MqttConnectOptions createMqttConnectOptionsDefault(MqttProperties.Client properties) {
        MqttConnectOptions options = new MqttConnectOptions();

        options.setServerURIs(properties.getUris());

        options.setMaxReconnectDelay((int) properties.getMaxReconnectDelay().toMillis());
        options.setKeepAliveInterval((int) properties.getKeepAliveInterval().toSeconds());
        options.setConnectionTimeout((int) properties.getConnectionTimeout().toSeconds());
        options.setExecutorServiceTimeout((int) properties.getExecutorServiceTimeout().toSeconds());

        options.setCleanSession(properties.isCleanSession());
        options.setAutomaticReconnect(properties.isAutomaticReconnect());

        if (properties.isAuthorizationPresent()) {
            options.setUserName(properties.getUsername());
            options.setPassword(properties.getPassword().toCharArray());
        }

        MqttProperties.Client.LastWillTestament lastWillTestament = properties.getLwt();
        if (lastWillTestament != null) {
            boolean valid = StringUtils.hasText(lastWillTestament.getTopic()) && StringUtils.hasText(lastWillTestament.getPayload());
            if (!valid) {
                throw new IllegalArgumentException("Last will defined for client but does not declare topic or payload. clientId=" + properties.getId() + ", lastWill=" + lastWillTestament);
            }
            options.setWill(lastWillTestament.getTopic(), lastWillTestament.getPayload().getBytes(StandardCharsets.UTF_8), lastWillTestament.getQos().getValue(), lastWillTestament.isRetained());
        }
        return options;
    }
}
