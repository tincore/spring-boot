package com.tincore.boot.mqtt;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.StringUtils;

import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@ConfigurationProperties(prefix = "mqtt")
public class MqttProperties {

    public static final String[] CLIENT_URI_DEFAULT = {"tcp://127.0.0.1:1883"};
    public static final Duration DEFAULT_AUTOSTART_DELAY = Duration.ofMinutes(1);

    /** Enable mqtt autoconfiguration. */
    @Builder.Default private boolean enabled = true;

    /** Autostart mqtt subscriptions. */
    @Builder.Default private boolean autostart = true;

    /** Autostart subscriptions delay */
    @Builder.Default private Duration autostartDelay = DEFAULT_AUTOSTART_DELAY;

    /** Client definitions. There should be at least one. The first one in the list is considered default. */
    @Builder.Default private List<Client> clients = new ArrayList<>();

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder(toBuilder = true)
    public static class Client {

        public static final Duration DEFAULT_MAX_RECONNECT_DELAY = Duration.ofSeconds(60);
        public static final Duration DEFAULT_KEEPALIVE_INTERVAL = Duration.ofSeconds(60);
        public static final Duration DEFAULT_CONNECTION_TIMEOUT = Duration.ofSeconds(30);
        public static final Duration DEFAULT_EXECUTOR_SERVICE_TIMEOUT = Duration.ofSeconds(10);
        /** Client Id */
        private String id;

        @Builder.Default private String[] uris = CLIENT_URI_DEFAULT;

        private String username;

        private String password;

        private Path persistenceDirectory;

        @Builder.Default private boolean enableSharedSubscription = true;

        @Builder.Default private MqttQoS publishQos = MqttQoS.AT_MOST_ONCE;

        @Builder.Default private MqttQoS subscribeQos = MqttQoS.AT_MOST_ONCE;

        /** Maximum time to wait between reconnects */
        @Builder.Default private Duration maxReconnectDelay = DEFAULT_MAX_RECONNECT_DELAY;

        @Builder.Default private Duration keepAliveInterval = DEFAULT_KEEPALIVE_INTERVAL;

        @Builder.Default private Duration connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;

        @Builder.Default private Duration executorServiceTimeout = DEFAULT_EXECUTOR_SERVICE_TIMEOUT;

        @Builder.Default private boolean cleanSession = true;

        @Builder.Default private boolean automaticReconnect = true;

        private LastWillTestament lwt;

        public boolean isAuthorizationPresent() {
            return StringUtils.hasText(username) && StringUtils.hasText(password);
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        @Builder(toBuilder = true)
        public static class LastWillTestament {

            private String topic;

            private String payload;

            private MqttQoS qos = MqttQoS.AT_MOST_ONCE;

            private boolean retained;
        }
    }
}
