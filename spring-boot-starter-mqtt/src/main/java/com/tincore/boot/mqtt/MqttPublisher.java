package com.tincore.boot.mqtt;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;

import java.nio.charset.StandardCharsets;

@Slf4j
@RequiredArgsConstructor
public class MqttPublisher {

    private final MqttClientManager mqttClientManager;
    private final MqttPayloadConversionService mqttPayloadConversionService;

    private byte[] getMessagePayloadBytes(Object payload) {
        Class<?> sourceType = payload.getClass();
        byte[] bytes;
        if (sourceType == byte[].class) {
            bytes = (byte[]) payload;
        } else if (mqttPayloadConversionService.canConvert(sourceType, byte[].class)) {
            bytes = mqttPayloadConversionService.convert(payload, byte[].class);
        } else if (mqttPayloadConversionService.canConvert(sourceType, String.class)) {
            bytes = mqttPayloadConversionService.convert(payload, String.class).getBytes(StandardCharsets.UTF_8);
        } else {
            throw new IllegalArgumentException("Could not convert payload to bytes. sourceType=" + sourceType);
        }
        return bytes;
    }

    public void publish(String topic, Object payload) throws MqttException {
        publish(topic, payload, mqttClientManager.getDefaultClientId());
    }

    public void publish(String topic, Object payload, IMqttActionListener callback) throws MqttException {
        publish(topic, payload, mqttClientManager.getDefaultClientId(), callback);
    }

    public void publish(String topic, Object payload, MqttQoS qos, boolean retained) throws MqttException {
        publish(topic, payload, mqttClientManager.getDefaultClientId(), qos, retained);
    }

    public void publish(String topic, Object payload, MqttQoS qos, boolean retained, IMqttActionListener callback) throws MqttException {
        publish(topic, payload, mqttClientManager.getDefaultClientId(), qos, retained, callback);
    }

    public void publish(String topic, Object payload, String clientId) throws MqttException {
        publish(topic, payload, clientId, mqttClientManager.getClientPropertiesByClientId(clientId).getPublishQos(), false, null);
    }

    public void publish(String topic, Object payload, String clientId, IMqttActionListener callback) throws MqttException {
        publish(topic, payload, clientId, mqttClientManager.getClientPropertiesByClientId(clientId).getPublishQos(), false, callback);
    }

    public void publish(String topic, Object payload, String clientId, MqttQoS qos, boolean retained) throws MqttException {
        publish(topic, payload, clientId, qos, retained, null);
    }

    public void publish(String topic, Object payload, String clientId, MqttQoS qos, boolean retained, IMqttActionListener callback) throws MqttException {
        MqttTopic.validate(topic, false);

        MqttMessage mqttMessage = new MqttMessage();
        if (payload != null) {
            mqttMessage.setPayload(getMessagePayloadBytes(payload));
        }
        mqttMessage.setQos(qos.getValue());
        mqttMessage.setRetained(retained);

        try {
            mqttClientManager.getClientByClientId(clientId).publish(topic, mqttMessage, null, callback);
        } catch (MqttException e) {
            log.error("Publish  failed. clientId={}, topic={}", clientId, topic, e);
            throw e;
        }
    }
}
