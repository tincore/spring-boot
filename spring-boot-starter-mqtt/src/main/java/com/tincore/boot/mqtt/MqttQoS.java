package com.tincore.boot.mqtt;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum MqttQoS {
    AT_MOST_ONCE(0),
    AT_LEAST_ONCE(1),
    EXACTLY_ONCE(2),
    DEFAULT(128);

    private static final Map<Integer, MqttQoS> VALUES = Stream.of(MqttQoS.values()).collect(Collectors.toMap(MqttQoS::getValue, v -> v));

    private final int value;

    MqttQoS(int value) {
        this.value = value;
    }

    public static MqttQoS byValue(int qos) {
        return VALUES.get(qos);
    }

    public int getValue() {
        return this.value;
    }
}
