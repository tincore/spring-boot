package com.tincore.boot.mqtt;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.eclipse.paho.client.mqttv3.MqttMessage;

public interface MqttReceivedMessageHandler {
    /**
     * This method is called when a message arrives from the server. This method is invoked synchronously by the MQTT client. An acknowledgment is not sent back to the server until this method returns cleanly. If an implementation of this
     * method throws an Exception, then the client will be shut down. When the client is next re-connected, any QoS 1 or 2 messages will be redelivered by the server. Any additional messages which arrive while an implementation of this
     * method is running, will build up in memory, and will then back up on the network. If an application needs to persist data, then it should ensure the data is persisted prior to returning from this method, as after returning from this
     * method, the message is considered to have been delivered, and will not be reproducible. It is possible to send a new message within an implementation of this callback (for example, a response to this message), but the implementation
     * must not disconnect the client, as it will be impossible to send an acknowledgment for the message being processed, and a deadlock will occur.
     *
     * @param topic name of the topic on the message was published to
     * @param mqttMessage the actual message
     * @param mqttSubscriberMapping
     * @param clientId
     */
    void onMessage(String topic, MqttMessage mqttMessage, MqttSubscriberMapping mqttSubscriberMapping, String clientId);
}
