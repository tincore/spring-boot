package com.tincore.boot.mqtt;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.mqtt.annotation.MqttMapping;
import com.tincore.boot.mqtt.annotation.MqttPathVariable;
import com.tincore.boot.mqtt.annotation.MqttPayload;
import com.tincore.boot.mqtt.annotation.MqttTopic;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.core.annotation.Order;
import org.springframework.core.convert.ConversionException;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@RequiredArgsConstructor
public class MqttSubscriberFactory {

    private final MqttPayloadConversionService mqttPayloadConversionService;

    private MqttSubscriber createMqttSubscriberByReflection(Object bean, Method method, MqttMapping mqttMapping) {
        List<MethodParameterMapping> methodParameterMappings = Stream.of(method.getParameters()).map(MethodParameterMapping::new).collect(Collectors.toList());

        Map<String, Class<?>> parameterTypes = methodParameterMappings.stream().filter(m -> m.getName() != null).collect(Collectors.toMap(MethodParameterMapping::getName, MethodParameterMapping::getType));
        List<MqttSubscriberMapping> mqttSubscriberMappings =
                Stream.of(mqttMapping.value())
                        .map(t -> new MqttSubscriberMapping(t, mqttMapping.qos(), mqttMapping.shared(), mqttMapping.group(), parameterTypes))
                        .sorted(Comparator.comparingInt(MqttSubscriberMapping::getParametrizedMappingParameterCount).reversed())
                        .collect(Collectors.toList());

        MqttReceivedMessageHandler messageHandler = new ReflectiveMttReceivedMessageHandler(method, bean, methodParameterMappings);

        MqttSubscriber mqttSubscriber = new MqttSubscriber(mqttSubscriberMappings, messageHandler, Set.of(mqttMapping.clients()));
        if (method.isAnnotationPresent(Order.class)) {
            mqttSubscriber.setOrder(method.getAnnotation(Order.class).value());
        }

        return mqttSubscriber;
    }

    public List<MqttSubscriber> createMqttSubscribersByReflection(Object bean, Method method) {
        MqttMapping[] mqttSubscribes = method.getAnnotationsByType(MqttMapping.class);
        return Stream.of(mqttSubscribes).map(a -> createMqttSubscriberByReflection(bean, method, a)).collect(Collectors.toList());
    }

    public Object fromMessagePayloadBytes(byte[] payloadBytes, Class<?> targetType, List<Converter<Object, Object>> customConverters) { // NOPMD complex
        if (payloadBytes == null) {
            return null;
        }

        Object payload = payloadBytes;
        if (customConverters != null) {
            for (Converter<Object, Object> converter : customConverters) {
                payload = converter.convert(payload);
                if (payload == null) {
                    return null;
                }
            }
        }

        Class<?> sourceType = payload.getClass();
        if (targetType == sourceType) {
            return payload;
        }

        if (mqttPayloadConversionService.canConvert(sourceType, targetType)) {
            return mqttPayloadConversionService.convert(payload, targetType);
        }

        if (mqttPayloadConversionService.canConvert(String.class, targetType) && mqttPayloadConversionService.canConvert(sourceType, String.class)) {
            String asString = mqttPayloadConversionService.convert(payload, String.class);
            return mqttPayloadConversionService.convert(asString, targetType);
        }

        throw new IllegalStateException("Unsupported payload conversion. targetType=" + targetType.getName());
    }

    @Slf4j
    @Data
    private static final class MethodParameterMapping {

        private static final String TOPIC_PARAMETER_NAME = "topic";
        private static final String PAYLOAD_PARAMETER_NAME = "payload";

        private final Class<?> type;
        private final String name;
        private final boolean required;
        private final boolean payloadBound;
        private final boolean topicBound;
        private final List<Converter<Object, Object>> converters;
        private final Object nullValue;

        MethodParameterMapping(Parameter parameter) {
            this.type = parameter.getType();
            this.nullValue = getNullValue(type);

            boolean required = parameter.getAnnotation(NonNull.class) != null;

            // IMPROVE: Add default value???
            MqttPathVariable mqttPathVariable = parameter.getAnnotation(MqttPathVariable.class);
            if (mqttPathVariable == null) {
                this.name = parameter.getName();
            } else {
                required = required || mqttPathVariable.required();
                this.name = mqttPathVariable.value();
            }

            MqttPayload mqttPayload = parameter.getAnnotation(MqttPayload.class);
            if (mqttPayload == null) {
                this.payloadBound = this.name.equals(PAYLOAD_PARAMETER_NAME);
                this.converters = null;
            } else {
                required = required || mqttPayload.required();
                this.payloadBound = true;
                this.converters = toConverters(mqttPayload.converters());
            }

            MqttTopic mqttTopic = parameter.getAnnotation(MqttTopic.class);
            this.topicBound = mqttTopic != null || this.name.equals(TOPIC_PARAMETER_NAME);

            this.required = required;
        }

        private static Object getNullValue(Class<?> type) { // NOPMD complex
            if (type.isPrimitive()) {
                if (type == boolean.class) {
                    return false;
                }
                if (type == char.class) {
                    return (char) 0;
                }
                if (type == byte.class) {
                    return (byte) 0;
                }
                if (type == short.class) {
                    return (short) 0;
                }
                if (type == int.class) {
                    return 0;
                }
                if (type == long.class) {
                    return 0L;
                }
                if (type == float.class) {
                    return 0.0f;
                }
                if (type == double.class) {
                    return 0.0d;
                }
            }
            return null;
        }

        @SuppressWarnings("unchecked")
        public static List<Converter<Object, Object>> toConverters(Class<? extends Converter<?, ?>>... classes) {
            if (classes == null || classes.length == 0) {
                return null;
            }

            return Stream.of(classes)
                    .map(
                            c -> {
                                try {
                                    return (Converter<Object, Object>) c.getDeclaredConstructor().newInstance();
                                } catch (ReflectiveOperationException e) {
                                    throw new IllegalArgumentException("Invalid converter declared", e);
                                }
                            })
                    .collect(Collectors.toList());
        }
    }

    private class ReflectiveMttReceivedMessageHandler implements MqttReceivedMessageHandler {
        private final Method method;
        private final Object bean;
        private final List<MethodParameterMapping> methodParameterMappings;
        private final boolean explicitPayloadParameterPresent;

        ReflectiveMttReceivedMessageHandler(Method method, Object bean, List<MethodParameterMapping> methodParameterMappings) {
            this.method = method;
            this.bean = bean;
            this.explicitPayloadParameterPresent = methodParameterMappings.stream().anyMatch(MethodParameterMapping::isPayloadBound);
            this.methodParameterMappings = methodParameterMappings;
        }

        @Override
        public void onMessage(String topic, MqttMessage mqttMessage, MqttSubscriberMapping mqttTopicDefinition, String clientId) {
            try {
                Object[] args = setupArgs(topic, mqttMessage, mqttTopicDefinition);
                method.invoke(bean, args);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | ConversionException e) {
                log.error("onMessage invocation failed", e);
                throw new IllegalStateException("onMessage invocation failed. clientId=" + clientId + ", topic=" + topic, e);
            }
        }

        private Object setupArg(String topic, MqttMessage mqttMessage, Map<String, String> pathValuesModel, MethodParameterMapping methodParameterMapping) {
            Class<?> targetType = methodParameterMapping.getType();

            if (targetType == MqttMessage.class) {
                return mqttMessage;
            }

            if (targetType.equals(MqttQoS.class)) {
                return MqttQoS.byValue(mqttMessage.getQos());
            }

            if (methodParameterMapping.isTopicBound()) {
                return topic;
            }

            String parameterName = methodParameterMapping.getName();
            if (pathValuesModel.containsKey(parameterName)) {
                // Set value on method parameter
                if (mqttPayloadConversionService.canConvert(String.class, targetType)) {
                    return mqttPayloadConversionService.convert(pathValuesModel.get(parameterName), targetType);
                } else {
                    log.warn("Cant convert path field into parameter. Ignoring. name={}, targetType={}", parameterName, targetType.getName());
                    return null;
                }
            }

            if (methodParameterMapping.isPayloadBound() || !explicitPayloadParameterPresent && Thread.currentThread().getContextClassLoader() != null) {
                return fromMessagePayloadBytes(mqttMessage.getPayload(), targetType, methodParameterMapping.getConverters());
            }

            return null;
        }

        private Object[] setupArgs(String topic, MqttMessage mqttMessage, MqttSubscriberMapping mqttTopicDefinition) {
            Map<String, String> pathValues = mqttTopicDefinition.extractPathValues(topic);
            return methodParameterMappings.stream()
                    .map(
                            p ->
                                    Optional.ofNullable(setupArg(topic, mqttMessage, pathValues, p))
                                            .orElseGet(
                                                    () -> {
                                                        if (p.isRequired()) {
                                                            throw new IllegalArgumentException("Required parameter value not present name=" + p);
                                                        }
                                                        return p.getNullValue();
                                                    }))
                    .toArray(Object[]::new);
        }
    }
}
