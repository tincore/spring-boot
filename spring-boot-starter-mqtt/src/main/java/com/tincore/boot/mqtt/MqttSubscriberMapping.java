package com.tincore.boot.mqtt;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MqttSubscriberMapping {
    private static final char PLACEHOLDER_BEGIN = '{';
    private static final char PLACEHOLDER_END = '}';

    private static final Pattern PATH_PLACEHOLDER_PATTERN = Pattern.compile("[^/]*\\{\\w+}[^/]*");

    private static final String REGEX_PART_STRING = "([^/]+)";
    private static final String REGEX_PART_NUMBER_WHOLE = "(\\d+?)";
    private static final String REGEX_PART_NUMBER_DECIMAL = "(\\d+(?:\\.\\d+)?)";

    private final String topicFilter;

    private final Pattern topicFilterPattern;
    private final List<String> topicFilterPatternParameterNames;

    private final MqttQoS qos;
    private final String sharedTopicFilterPrefix;

    public MqttSubscriberMapping(String topicFilterExpression, MqttQoS qos, boolean shared, String group, Map<String, Class<?>> paramTypes) { // NOPMD Complex
        boolean parametrized = topicFilterExpression.indexOf(PLACEHOLDER_BEGIN) >= 0;
        if (parametrized) {
            this.topicFilterPatternParameterNames = new ArrayList<>();

            StringBuilder regexStringBuffer = new StringBuilder("^");
            StringBuilder currentParameterName = null;
            for (char c : topicFilterExpression.toCharArray()) {
                switch (c) {
                    case '.':
                    case '*':
                    case '?':
                    case ':':
                    case '^':
                    case '$':
                    case '|':
                    case '(':
                    case ')':
                    case '[':
                    case ']':
                    case '\\':
                        regexStringBuffer.append('\\').append(c);
                        break;
                    case '+':
                        regexStringBuffer.append("(?:[^/]+)");
                        break;
                    case '#':
                        regexStringBuffer.append(".*");
                        break;
                    case PLACEHOLDER_BEGIN:
                        currentParameterName = new StringBuilder(); // NOPMD
                        break;
                    case PLACEHOLDER_END:
                        if (currentParameterName == null) {
                            throw new IllegalArgumentException("Malformed topic expression. Closed placeholder without open. Expression=" + topicFilterExpression);
                        }
                        String parameterName = currentParameterName.toString();
                        topicFilterPatternParameterNames.add(parameterName);

                        Class<?> paramType = paramTypes.get(parameterName);
                        //                        Class<?> paramType = paramTypes.getOrDefault(parameterName, String.class);

                        if (paramType == null) {
                            throw new IllegalArgumentException("Malformed topic expression. Can not find parameter with name=" + parameterName + ".in " + paramTypes);
                        }

                        if (Integer.class.isAssignableFrom(paramType) || Long.class.isAssignableFrom(paramType) || paramType == int.class || paramType == long.class) {
                            regexStringBuffer.append(REGEX_PART_NUMBER_WHOLE);
                        } else if (Number.class.isAssignableFrom(paramType) || paramType == double.class || paramType == float.class) {
                            regexStringBuffer.append(REGEX_PART_NUMBER_DECIMAL);
                        } else {
                            regexStringBuffer.append(REGEX_PART_STRING);
                        }

                        currentParameterName = null;
                        break;
                    default:
                        if (currentParameterName == null) {
                            regexStringBuffer.append(c);
                        } else {
                            currentParameterName.append(c);
                        }
                        break;
                }
            }
            regexStringBuffer.append("$");

            this.topicFilterPattern = Pattern.compile(regexStringBuffer.toString());

            this.topicFilter = PATH_PLACEHOLDER_PATTERN.matcher(topicFilterExpression).replaceAll("+");
        } else {
            this.topicFilter = topicFilterExpression;
            this.topicFilterPattern = null;
            this.topicFilterPatternParameterNames = null;
        }

        this.qos = qos;

        if (shared) {
            sharedTopicFilterPrefix = StringUtils.hasText(group) ? "$share/" + group + "/" : "$queue/";
        } else {
            sharedTopicFilterPrefix = null;
        }

        MqttTopic.validate(topicFilter, true);
    }

    public MqttSubscriberMapping(String topicFilterExpression, MqttQoS qos, boolean shared, String group) {
        this(topicFilterExpression, qos, shared, group, Collections.emptyMap());
    }

    public MqttSubscriberMapping(String topicFilterExpression, MqttQoS qos) {
        this(topicFilterExpression, qos, false, null, Collections.emptyMap());
    }

    public MqttSubscriberMapping(String topicFilterExpression) {
        this(topicFilterExpression, MqttQoS.DEFAULT, false, null, Collections.emptyMap());
    }

    public Map<String, String> extractPathValues(String topic) {
        if (isParametrizedMapping()) {
            Matcher matcher = topicFilterPattern.matcher(topic);
            if (matcher.find()) {
                return IntStream.range(0, topicFilterPatternParameterNames.size()).boxed().collect(Collectors.toMap(topicFilterPatternParameterNames::get, i -> matcher.group(i + 1)));
            }
        }
        return Collections.emptyMap();
    }

    public int getParametrizedMappingParameterCount() {
        return this.topicFilterPatternParameterNames == null ? 0 : this.topicFilterPatternParameterNames.size();
    }

    public MqttQoS getQos(MqttQoS defaultSubscribeQos) {
        return qos == MqttQoS.DEFAULT ? defaultSubscribeQos : qos;
    }

    public String getTopicFilter(boolean enableSharedSubscription) {
        return !enableSharedSubscription || this.sharedTopicFilterPrefix == null ? this.topicFilter : this.sharedTopicFilterPrefix + this.topicFilter;
    }

    public boolean isParametrizedMapping() {
        return this.topicFilterPattern != null;
    }

    public boolean isTopicMatch(String topic) {
        return isParametrizedMapping() ? topicFilterPattern.matcher(topic).matches() : MqttTopic.isMatched(this.topicFilter, topic);
    }
}
