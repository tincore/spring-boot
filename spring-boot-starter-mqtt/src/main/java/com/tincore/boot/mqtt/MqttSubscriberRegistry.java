package com.tincore.boot.mqtt;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class MqttSubscriberRegistry {

    private final List<MqttSubscriber> mqttSubscribers = new CopyOnWriteArrayList<>();

    public void addMqttSubscriber(MqttSubscriber mqttSubscriber) {
        this.mqttSubscribers.add(mqttSubscriber);
    }

    public void addMqttSubscribers(Collection<MqttSubscriber> mqttSubscribers) {
        this.mqttSubscribers.addAll(mqttSubscribers);
    }

    public List<MqttSubscriber> getMqttSubscribers() {
        return mqttSubscribers;
    }

    public void removeAllMqttSubscribers(Collection<MqttSubscriber> mqttSubscribers) {
        this.mqttSubscribers.removeAll(mqttSubscribers);
    }

    public void removeMqttSubscriber(MqttSubscriber mqttSubscriber) {
        this.mqttSubscribers.remove(mqttSubscriber);
    }
}
