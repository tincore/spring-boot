package com.tincore.boot.mqtt.autoconfigure;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.mqtt.*;
import org.eclipse.paho.client.mqttv3.MqttAsyncClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.eclipse.paho.client.mqttv3.persist.MqttDefaultFilePersistence;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

@AutoConfigureAfter(MqttPayloadConversionAutoConfiguration.class)
@ConditionalOnClass(MqttAsyncClient.class)
@ConditionalOnProperty(prefix = "mqtt", name = "enabled", havingValue = "true", matchIfMissing = true)
@EnableConfigurationProperties({MqttProperties.class})
@Configuration
public class MqttAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(MqttAsyncClientFactory.class)
    public MqttAsyncClientFactory mqttAsyncClientFactory() {
        return (clientId, persistenceDirectory, serverURI) -> new MqttAsyncClient(serverURI[0], clientId, persistenceDirectory == null ? new MemoryPersistence() : new MqttDefaultFilePersistence(persistenceDirectory.toString()));
    }

    @Bean
    @ConditionalOnMissingBean(MqttConnectOptionsFactory.class)
    public MqttConnectOptionsFactory mqttConnectOptionsFactory() {
        return new MqttConnectOptionsFactory() {
            @Override
            public MqttConnectOptions createMqttConnectOptions(MqttProperties.Client clientProperties) {
                return createMqttConnectOptionsDefault(clientProperties);
            }
        };
    }

    @Bean
    @Order // Leave for last to facilitate autostart
    public MqttClientManager mqttClientManager(MqttAsyncClientFactory mqttAsyncClientFactory, MqttProperties mqttProperties, MqttConnectOptionsFactory mqttConnectOptionsFactory, MqttSubscriberRegistry mqttSubscriberRegistry) {
        MqttClientManager mqttClientManager = new MqttClientManager(mqttProperties, mqttAsyncClientFactory, mqttConnectOptionsFactory, mqttSubscriberRegistry);
        if (mqttProperties.isAutostart()) {
            mqttClientManager.connectAll(mqttProperties.getAutostartDelay());
        }
        return mqttClientManager;
    }

    @Bean
    public MqttPayloadConversionService mqttPayloadConversionService(MqttPayloadWriteConverter mqttPayloadWriteConverter, MqttPayloadReadConverterFactory mqttPayloadReadConverterFactory) {
        return new MqttPayloadConversionService(mqttPayloadWriteConverter, mqttPayloadReadConverterFactory);
    }

    @Bean
    @ConditionalOnMissingBean(MqttPublisher.class)
    public MqttPublisher mqttPublisher(MqttClientManager mqttClientManager, MqttPayloadConversionService mqttPayloadConversionService) {
        return new MqttPublisher(mqttClientManager, mqttPayloadConversionService);
    }

    @Bean
    public MqttSubscriberBeanPostProcessor mqttSubscriberBeanPostProcessor(MqttSubscriberFactory mqttSubscriberFactory, MqttSubscriberRegistry mqttSubscriberRegistry) {
        return new MqttSubscriberBeanPostProcessor(mqttSubscriberRegistry, mqttSubscriberFactory);
    }

    @Bean
    public MqttSubscriberFactory mqttSubscriberFactory(MqttPayloadConversionService mqttPayloadConversionService) {
        return new MqttSubscriberFactory(mqttPayloadConversionService);
    }

    @Bean
    public MqttSubscriberRegistry mqttSubscriberRegistry() {
        return new MqttSubscriberRegistry();
    }
}
