package com.tincore.boot.mqtt.autoconfigure;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.tincore.boot.mqtt.MqttPayloadReadConverterFactory;
import com.tincore.boot.mqtt.MqttPayloadWriteConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

import java.io.IOException;

@Slf4j
@AutoConfigureAfter({JacksonAutoConfiguration.class})
@ConditionalOnClass(ObjectMapper.class)
@Configuration
public class MqttPayloadConversionAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean(MqttPayloadReadConverterFactory.class)
    public MqttPayloadReadConverterFactory mqttPayloadReadConverterFactory(ObjectMapper objectMapper) {
        return new MqttPayloadReadConverterFactory() {
            @Override
            @SuppressWarnings("unchecked")
            public <T> Converter<byte[], T> getConverter(Class<T> target) {
                return s -> {
                    try {
                        return objectMapper.readValue(s, target);
                    } catch (IOException e) {
                        throw new IllegalArgumentException("Type could not be converted to type=" + target + " reason=" + e.getMessage(), e);
                    }
                };
            }
        };
    }

    @Bean
    @ConditionalOnMissingBean(MqttPayloadWriteConverter.class)
    public MqttPayloadWriteConverter mqttPayloadWriteConverter(ObjectMapper objectMapper) {
        return s -> {
            try {
                return objectMapper.writeValueAsBytes(s);
            } catch (IOException e) {
                throw new IllegalArgumentException("Value could not be converted to bytes. reason=" + e.getMessage(), e);
            }
        };
    }

    @Bean
    @ConditionalOnMissingBean(ObjectMapper.class)
    public ObjectMapper objectMapper() {
        return new ObjectMapper()
                .setSerializationInclusion(JsonInclude.Include.NON_NULL)
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES)
                .registerModule(new ParameterNamesModule())
                .registerModule(new Jdk8Module())
                .registerModule(new JavaTimeModule())
                .findAndRegisterModules();
    }
}
