package com.tincore.boot.mqtt;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.mqtt.test.support.AbstractMqttApplicationTests;
import com.tincore.boot.mqtt.test.support.TestMqttMessageCollector;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.junit.jupiter.api.Test;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
class ApplicationTests extends AbstractMqttApplicationTests {

    @Test
    void testMqttServerGivenStartedAndPublisherAndSubscriberOnSameTopicWhenPublisherSendsPayloadThenSubscriberReceivesPayload() throws Exception {
        String messagePayload = "Sanity MQTT Test";
        String topic = "a1/b1";

        try (MqttClient testPublisher = createPahoMqttClient("testSanityPublisher");
                MqttClient testSubscriber = createPahoMqttClient("testSanitySubscriber")) {

            TestMqttMessageCollector collector = new TestMqttMessageCollector();
            testSubscriber.setCallback(collector);
            testSubscriber.connect();
            testSubscriber.subscribe(topic, MqttQoS.AT_LEAST_ONCE.getValue());

            testPublisher.connect();
            testPublisher.publish(topic, messagePayload.getBytes(UTF_8), 2, false);

            awaitMqtt().until(collector::isMessageReceived);

            TestMqttMessageCollector.ReceivedMessage receivedMqttMessage = collector.getMessage();
            assertThat(receivedMqttMessage.getPayload()).isEqualTo(messagePayload.getBytes(UTF_8));
            assertThat(receivedMqttMessage.getQos()).isEqualTo(MqttQoS.AT_LEAST_ONCE);

            testSubscriber.disconnect();
            testPublisher.disconnect();
        }
    }
}
