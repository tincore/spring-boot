package com.tincore.boot.mqtt;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.mqtt.test.support.TestFixtureTrait;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class MqttClientManagerTest implements TestFixtureTrait {

    public static final String CLIENT_ID = "c1";

    @Mock private MqttAsyncClientFactory mockMqttAsyncClientFactory;
    @Mock private MqttConnectOptionsFactory mockMqttConnectOptionsFactory;
    @Mock private MqttSubscriberRegistry mockMqttSubscriberRegistry;
    @Mock private MqttReceivedMessageHandler mockMessageHandler;

    public MqttClientManager createMqttClientManager() {
        MqttProperties mqttProperties = createMqttProperties();
        return new MqttClientManager(mqttProperties, mockMqttAsyncClientFactory, mockMqttConnectOptionsFactory, mockMqttSubscriberRegistry);
    }

    @Test
    void testCreateClientSubscriptionProjectGivenMappingsWithMpOverlappingPathThenReturnsEachMapping() {
        MqttClientManager mqttClientManager = createMqttClientManager();

        MqttProperties.Client clientProperties = new MqttProperties.Client();
        MqttClientManager.ClientSubscriptionProject subscriptionProject =
                mqttClientManager.createClientSubscriptionProject(CLIENT_ID, clientProperties, List.of(new MqttSubscriber(createMqttSubscriberMappings("a/b", "a/b/c1", "a/b/c2"), mockMessageHandler, Set.of(CLIENT_ID))));

        assertThat(subscriptionProject.getEntries())
                .hasSize(3)
                .anySatisfy(e -> assertThat(e.getTopicFilter()).isEqualTo("a/b"))
                .anySatisfy(e -> assertThat(e.getTopicFilter()).isEqualTo("a/b/c1"))
                .anySatisfy(e -> assertThat(e.getTopicFilter()).isEqualTo("a/b/c2"));
    }

    @Test
    void testCreateClientSubscriptionProjectGivenMappingsWithOverlappingPathThenReturnsSingleProjectEntryWithMostGeneralMapping() {
        MqttClientManager mqttClientManager = createMqttClientManager();

        MqttProperties.Client clientProperties = new MqttProperties.Client();
        MqttClientManager.ClientSubscriptionProject subscriptionProject =
                mqttClientManager.createClientSubscriptionProject(CLIENT_ID, clientProperties, List.of(new MqttSubscriber(createMqttSubscriberMappings("a/b", "a/#", "a/b/c", "a/+/c"), mockMessageHandler, Set.of(CLIENT_ID))));

        assertThat(subscriptionProject.getEntries()).singleElement().satisfies(e -> assertThat(e.getTopicFilter()).isEqualTo("a/#"));
    }

    @Test
    void testCreateClientSubscriptionProjectGivenMappingsWithSamePathThenReturnsSingleProjectEntry() {
        MqttClientManager mqttClientManager = createMqttClientManager();

        MqttProperties.Client clientProperties = new MqttProperties.Client();
        MqttClientManager.ClientSubscriptionProject subscriptionProject =
                mqttClientManager.createClientSubscriptionProject(
                        CLIENT_ID,
                        clientProperties,
                        List.of(new MqttSubscriber(createMqttSubscriberMappings("a/b", "a/b"), mockMessageHandler, Set.of(CLIENT_ID)), new MqttSubscriber(createMqttSubscriberMappings("a/b", "a/b"), mockMessageHandler, Set.of(CLIENT_ID))));

        assertThat(subscriptionProject.getEntries()).singleElement().satisfies(e -> assertThat(e.getTopicFilter()).isEqualTo("a/b"));
    }
}
