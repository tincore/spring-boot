package com.tincore.boot.mqtt;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tincore.boot.mqtt.annotation.MqttMapping;
import com.tincore.boot.mqtt.annotation.MqttPayload;
import com.tincore.boot.mqtt.test.support.AbstractMqttApplicationTests;
import com.tincore.boot.mqtt.test.support.TestMqttMessageCollector;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.core.convert.ConversionFailedException;

import java.nio.charset.StandardCharsets;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

@Slf4j
@Import({MqttPublisherIntegrationTests.TestStringMqttMessageHandler.class, MqttPublisherIntegrationTests.TestJsonMqttMessageHandler.class})
class MqttPublisherIntegrationTests extends AbstractMqttApplicationTests {

    public static final String TOPIC_STRING = "test/string";
    public static final String TOPIC_JSON = "test/json";

    @Autowired private TestJsonMqttMessageHandler testJsonMqttMessageHandler;
    @Autowired private TestStringMqttMessageHandler testStringMqttMessageHandler;

    @Autowired private ObjectMapper objectMapper;

    private void assertMessageStringReceivedSuccessfully(String expectedMessagePayload) throws InterruptedException {
        awaitMqtt().until(() -> testStringMqttMessageHandler.collector.isMessageReceived());

        assertThat(testStringMqttMessageHandler.collector.getMessage())
                .satisfies(
                        m -> {
                            assertThat(m.getPayload()).asString().isEqualTo(expectedMessagePayload);
                            assertThat(m.getQos()).isEqualTo(MqttQoS.AT_MOST_ONCE);
                        });
    }

    private String createMessagePayload() {
        return "Some test data" + System.nanoTime();
    }

    private TestPojo createTestPojo() {
        TestPojo messagePayload = new TestPojo();
        messagePayload.setPropertyA("propA");
        messagePayload.setPropertyB("propB");
        messagePayload.setPropertyC(123);
        return messagePayload;
    }

    @Test
    void testPublishGivenNullPayloadThenSubscriberReceivesPayloadAsEmptyString() throws Exception {
        mqttPublisher.publish(TOPIC_STRING, null);
        assertMessageStringReceivedSuccessfully("");
    }

    @Test
    void testPublishGivenSubscriberTopicEqualsAndObjectPayloadWhenJsonPayloadThenSubscriberReceivesJsonPayload() throws Exception {
        TestPojo pojo = createTestPojo();
        mqttPublisher.publish(TOPIC_JSON, pojo);

        awaitMqtt().until(() -> testJsonMqttMessageHandler.collectorPojo.isMessageReceived());

        TestMqttMessageCollector.ReceivedMessage receivedMqttMessage = testJsonMqttMessageHandler.collectorPojo.getMessage();
        assertThat(receivedMqttMessage.getQos()).isEqualTo(MqttQoS.AT_MOST_ONCE);
        assertThat(receivedMqttMessage.getPayload()).isEqualTo(pojo);
    }

    @Test
    void testPublishGivenSubscriberTopicEqualsAndStringPayloadWhenJsonPayloadThenSubscriberReceivesJsonPayload() throws Exception {
        TestPojo pojo = createTestPojo();
        mqttPublisher.publish(TOPIC_JSON, pojo);

        awaitMqtt().until(() -> testJsonMqttMessageHandler.collectorPojo.isMessageReceived());

        TestMqttMessageCollector.ReceivedMessage receivedMqttMessage = testJsonMqttMessageHandler.collectorString.getMessage();
        assertThat(receivedMqttMessage.getQos()).isEqualTo(MqttQoS.AT_MOST_ONCE);
        assertThat(receivedMqttMessage.getPayload()).isEqualTo(objectMapper.writeValueAsString(pojo));
    }

    @Test
    void testPublishGivenSubscriberTopicEqualsWhenBytesPayloadThenSubscriberReceivesStringPayloadAsString() throws Exception {
        String messagePayload = createMessagePayload();

        mqttPublisher.publish(TOPIC_STRING, messagePayload.getBytes(StandardCharsets.UTF_8));

        assertMessageStringReceivedSuccessfully(messagePayload);
    }

    @Test
    void testPublishGivenSubscriberTopicEqualsWhenNumberPayloadThenSubscriberReceivesPayloadAsString() throws Exception {
        mqttPublisher.publish(TOPIC_STRING, 1);

        assertMessageStringReceivedSuccessfully("1");
    }

    @Test
    void testPublishGivenSubscriberTopicEqualsWhenPayloadCantBeConvertedToObjectThenThrowsConversionFailedException() {
        assertThatExceptionOfType(ConversionFailedException.class).isThrownBy(() -> mqttPublisher.publish(TOPIC_STRING, new Object()));
    }

    @Test
    void testPublishGivenSubscriberTopicEqualsWhenStringPayloadAndMqttExactlyOnceAndRetainThenSubscriberReceivesPayloadAsString() throws Exception {
        String messagePayload = createMessagePayload();

        mqttPublisher.publish(TOPIC_STRING, messagePayload, MqttQoS.AT_LEAST_ONCE, true);

        assertMessageStringReceivedSuccessfully(messagePayload);
    }

    @Test
    void testPublishGivenSubscriberTopicEqualsWhenStringPayloadAndQosAtLeastOnceAndRetainedThenSubscriberReceivesPayloadAsStringAndCallbackNotified() throws Exception {
        String messagePayload = createMessagePayload();

        AtomicBoolean success = new AtomicBoolean();
        mqttPublisher.publish(
                TOPIC_STRING,
                messagePayload,
                MqttQoS.AT_LEAST_ONCE,
                true,
                new IMqttActionListener() {
                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {}

                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        success.set(true);
                    }
                });

        assertMessageStringReceivedSuccessfully(messagePayload);
        assertThat(success).isTrue();
    }

    @Test
    void testPublishGivenSubscriberTopicEqualsWhenStringPayloadThenSubscriberReceivesPayloadAsString() throws Exception {
        String messagePayload = createMessagePayload();

        mqttPublisher.publish(TOPIC_STRING, messagePayload);

        assertMessageStringReceivedSuccessfully(messagePayload);
    }

    @Test
    void testPublishGivenSubscriberTopicEqualsWhenStringPayloadThenSubscriberReceivesPayloadAsStringAndCallbackNotified() throws Exception {
        String messagePayload = createMessagePayload();

        AtomicBoolean success = new AtomicBoolean();
        mqttPublisher.publish(
                TOPIC_STRING,
                messagePayload,
                new IMqttActionListener() {
                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {}

                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        success.set(true);
                    }
                });

        assertMessageStringReceivedSuccessfully(messagePayload);
        assertThat(success).isTrue();
    }

    @Data
    public static class TestPojo {
        private String propertyA;
        private String propertyB;
        private int propertyC;
    }

    @Slf4j
    public static class TestJsonMqttMessageHandler {

        public final TestMqttMessageCollector collectorPojo = new TestMqttMessageCollector();
        public final TestMqttMessageCollector collectorString = new TestMqttMessageCollector();

        @MqttMapping(value = TOPIC_JSON, qos = MqttQoS.AT_MOST_ONCE)
        public void onMessageAsPojo(String topic, MqttMessage message, @MqttPayload TestPojo payload) {
            collectorPojo.addReceivedMessage(topic, message, payload);
        }

        @MqttMapping(value = TOPIC_JSON, qos = MqttQoS.AT_MOST_ONCE)
        public void onMessageAsString(String topic, MqttMessage message, String payload) {
            collectorString.addReceivedMessage(topic, message, payload);
        }
    }

    @Slf4j
    public static class TestStringMqttMessageHandler {

        public final TestMqttMessageCollector collector = new TestMqttMessageCollector();

        @MqttMapping(TOPIC_STRING)
        public void onMessage(String topic, MqttMessage message, @MqttPayload String payload) {
            collector.addReceivedMessage(topic, message, payload);
        }
    }
}
