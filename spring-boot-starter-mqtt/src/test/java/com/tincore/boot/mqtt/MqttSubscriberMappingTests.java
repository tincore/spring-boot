package com.tincore.boot.mqtt;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 Tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.mqtt.test.support.TestFixtureTrait;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;

@TestMethodOrder(MethodOrderer.MethodName.class)
public class MqttSubscriberMappingTests implements TestFixtureTrait {

    @Test
    public void testConstructGivenEmptyTopicThenThrowsException() {
        assertThatIllegalArgumentException().isThrownBy(() -> createMqttSubscriberMapping("", null));
    }

    @Test
    public void testCreateGivenTopicFilterWithPlaceholderAndPlaceholderNameNotInParameterTypesThenThrowsIllegalArgumentException() {
        assertThatIllegalArgumentException().isThrownBy(() -> createMqttSubscriberMapping("{param1}/+/{param2}", Map.of("param1", String.class, "NOparam2", String.class)));
    }

    @Test
    public void testExtractPathValuesGivenTopicFilterWithDecimalPlaceholdersWhenTopicHasSamePathThenReturnsTrue() {
        MqttSubscriberMapping mqttSubscriberMapping = createMqttSubscriberMapping("{param1}", Map.of("param1", BigDecimal.class));
        assertThat(mqttSubscriberMapping.extractPathValues("xxx")).isEmpty();
        assertThat(mqttSubscriberMapping.extractPathValues("123")).hasSize(1).containsEntry("param1", "123");
        assertThat(mqttSubscriberMapping.extractPathValues("12.3")).hasSize(1).containsEntry("param1", "12.3");
    }

    @Test
    public void testExtractPathValuesGivenTopicFilterWithLongPlaceholdersWhenTopicHasSamePathThenReturnsTrue() {
        MqttSubscriberMapping mqttSubscriberMapping = createMqttSubscriberMapping("{param1}", Map.of("param1", Long.class));

        assertThat(mqttSubscriberMapping.extractPathValues("xxx")).isEmpty();
        assertThat(mqttSubscriberMapping.extractPathValues("123")).hasSize(1).containsEntry("param1", "123");
    }

    @Test
    public void testExtractPathValuesGivenTopicFilterWithLongPrimitivePlaceholdersWhenTopicHasSamePathThenReturnsTrue() {
        MqttSubscriberMapping mqttSubscriberMapping = createMqttSubscriberMapping("{param1}", Map.of("param1", long.class));

        assertThat(mqttSubscriberMapping.extractPathValues("xxx")).isEmpty();
        assertThat(mqttSubscriberMapping.extractPathValues("123")).hasSize(1).containsEntry("param1", "123");
    }

    @Test
    public void testExtractPathValuesGivenTopicFilterWithNumericAndNotNumericPlaceholdersWhenTopicHasSamePathThenReturnsTrue() {
        MqttSubscriberMapping mqttSubscriberMapping = createMqttSubscriberMapping("{param1}/{param2}", Map.of("param1", Long.class, "param2", String.class));
        Map<String, String> map = mqttSubscriberMapping.extractPathValues("123/value2");

        assertThat(map).hasSize(2).containsEntry("param1", "123").containsEntry("param2", "value2");
    }

    @Test
    public void testExtractPathValuesGivenTopicFilterWithPlaceholderAndSigleLevelWildcardWhenTopicHasSamePathThenReturnsParameter() {
        assertThat(createMqttSubscriberMapping("+/{param}/#", Map.of("param", String.class)).extractPathValues("NO/value/other/other")).hasSize(1).containsEntry("param", "value");

        assertThat(createMqttSubscriberMapping("{param}/+/#", Map.of("param", String.class)).extractPathValues("value/NO/other/other")).hasSize(1).containsEntry("param", "value");

        assertThat(createMqttSubscriberMapping("{param1}/+/{param2}", Map.of("param1", String.class, "param2", String.class)).extractPathValues("value1/NO/value2"))
                .hasSize(2)
                .containsEntry("param1", "value1")
                .containsEntry("param2", "value2");
    }

    @Test
    public void testIsTopicMatchTopicGivenTopicFilterWithMultiLevelWildcardWhenAnyTopicReturnsTrue() {
        MqttSubscriberMapping mqttSubscriberMapping = createMqttSubscriberMapping("#", new HashMap<>());
        assertThat(mqttSubscriberMapping.isTopicMatch("level1/level2/level3/value1/otherSubLevel1/otherSubLevel2")).isTrue();
    }

    @Test
    public void testIsTopicMatchTopicGivenTopicFilterWithNumericAndNotNumericPlaceholdersWhenTopicHasSamePathThenReturnsTrue() {
        MqttSubscriberMapping mqttSubscriberMapping = createMqttSubscriberMapping("{param1}/{param2}", Map.of("param1", Long.class, "param2", String.class));

        assertThat(mqttSubscriberMapping.isTopicMatch("1.23/value2")).isFalse();
        assertThat(mqttSubscriberMapping.isTopicMatch("1.23")).isFalse();
        assertThat(mqttSubscriberMapping.isTopicMatch("value2/123")).isFalse();

        assertThat(mqttSubscriberMapping.isTopicMatch("123/value2")).isTrue();
    }

    @Test
    public void testIsTopicMatchTopicGivenTopicFilterWithPlaceholderAndMultiLevelWildcardWhenTopicHasSamePathThenReturnsTrue() {
        MqttSubscriberMapping mqttSubscriberMapping = createMqttSubscriberMapping("level1/level2/level3/{param}/#", Map.of("param", String.class));

        assertThat(mqttSubscriberMapping.isTopicMatch("value1")).isFalse();
        assertThat(mqttSubscriberMapping.isTopicMatch("level1/level2/level3/value1")).isFalse();
        assertThat(mqttSubscriberMapping.isTopicMatch("level1/level2/NOLEVEL3/value1/otherSubLevel1")).isFalse();

        assertThat(mqttSubscriberMapping.isTopicMatch("level1/level2/level3/value1/otherSubLevel1")).isTrue();
        assertThat(mqttSubscriberMapping.isTopicMatch("level1/level2/level3/value1/otherSubLevel1/otherSubLevel2")).isTrue();
    }

    @Test
    public void testIsTopicMatchTopicGivenTopicFilterWithPlaceholderAndSigleLevelWildcardWhenTopicHasSamePathThenReturnsTrue() {
        MqttSubscriberMapping mqttSubscriberMapping = createMqttSubscriberMapping("level1/+/level3/{param}/#", Map.of("param", String.class));

        assertThat(mqttSubscriberMapping.isTopicMatch("value1")).isFalse();
        assertThat(mqttSubscriberMapping.isTopicMatch("level1/level2/level3/value1")).isFalse();
        assertThat(mqttSubscriberMapping.isTopicMatch("level1/NOLEVEL2/NOLEVEL3/value1/otherSubLevel1")).isFalse();

        assertThat(mqttSubscriberMapping.isTopicMatch("level1/NOLEVEL2/level3/value1/otherSubLevel1")).isTrue();
        assertThat(mqttSubscriberMapping.isTopicMatch("level1/level2/level3/value1/otherSubLevel1")).isTrue();
        assertThat(mqttSubscriberMapping.isTopicMatch("level1/level2/level3/value1/otherSubLevel1/otherSubLevel2")).isTrue();
    }
}
