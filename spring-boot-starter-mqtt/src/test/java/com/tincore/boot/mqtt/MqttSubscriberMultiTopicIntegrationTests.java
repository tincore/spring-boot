package com.tincore.boot.mqtt;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.mqtt.annotation.MqttMapping;
import com.tincore.boot.mqtt.annotation.MqttPayload;
import com.tincore.boot.mqtt.annotation.MqttTopic;
import com.tincore.boot.mqtt.test.support.AbstractMqttApplicationTests;
import com.tincore.boot.mqtt.test.support.TestMqttMessageCollector;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@Import(MqttSubscriberMultiTopicIntegrationTests.TestMqttMessageHandlerDouble.class)
class MqttSubscriberMultiTopicIntegrationTests extends AbstractMqttApplicationTests {

    public static final String TOPIC_STRING_A = "testTopic/topicStringA";
    public static final String TOPIC_STRING_B = "testTopic/topicStringB";
    public static final String TOPIC_STRING_C = "testTopic/topicStringC";

    @Autowired private TestMqttMessageHandlerDouble testMqttMessageHandlerDouble;

    @Test
    void testSubscriptionGivenMultipleTopicWhenPublisherSendsStringPayloadAndSameTopicThenSubscriberReceivesStringPayload() throws Exception {
        try (MqttClient testPublisher = createTestMqttPublisher()) {
            String messagePayloadB = "TestB";
            testPublisher.publish(TOPIC_STRING_B, messagePayloadB.getBytes(UTF_8), MqttQoS.EXACTLY_ONCE.getValue(), false);

            String messagePayloadC = "TestC";
            testPublisher.publish(TOPIC_STRING_C, messagePayloadC.getBytes(UTF_8), MqttQoS.EXACTLY_ONCE.getValue(), false);

            awaitMqtt().until(() -> testMqttMessageHandlerDouble.collector.isMessageReceived());

            TestMqttMessageCollector.ReceivedMessage receivedMqttMessageB = testMqttMessageHandlerDouble.collector.getMessage();
            assertThat(receivedMqttMessageB.getPayload()).asString().isEqualTo(messagePayloadB);
            assertThat(receivedMqttMessageB.getQos()).isEqualTo(MqttQoS.AT_MOST_ONCE);

            TestMqttMessageCollector.ReceivedMessage receivedMqttMessageC = testMqttMessageHandlerDouble.collector.getMessage();
            assertThat(receivedMqttMessageC.getPayload()).asString().isEqualTo(messagePayloadC);
            assertThat(receivedMqttMessageC.getQos()).isEqualTo(MqttQoS.AT_LEAST_ONCE);

            testPublisher.disconnect();
        }
    }

    @Slf4j
    public static class TestMqttMessageHandlerDouble {

        public final TestMqttMessageCollector collector = new TestMqttMessageCollector();

        @MqttMapping(value = TOPIC_STRING_B)
        @MqttMapping(value = TOPIC_STRING_C, qos = MqttQoS.AT_LEAST_ONCE)
        public void someSubscriptionWithMultipleTopics(@MqttTopic String topicOtherName, MqttMessage message, @MqttPayload String payload) {
            collector.addReceivedMessage(topicOtherName, message, payload);
        }
    }
}
