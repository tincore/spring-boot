package com.tincore.boot.mqtt;

/*-
 * #%L
 * recipeton-cookbook-service
 * %%
 * Copyright (C) 2021 tincomisc
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.mqtt.annotation.MqttMapping;
import com.tincore.boot.mqtt.annotation.MqttPayload;
import com.tincore.boot.mqtt.test.support.AbstractMqttApplicationTests;
import com.tincore.boot.mqtt.test.support.TestMqttMessageCollector;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@Import(MqttSubscriberSingleTopicIntegrationTests.TestMqttMessageHandlerSingle.class)
class MqttSubscriberSingleTopicIntegrationTests extends AbstractMqttApplicationTests {

    public static final String TOPIC_STRING_A = "testTopic/topicStringA";

    @Autowired private TestMqttMessageHandlerSingle testMqttMessageHandlerSingle;

    @Test
    void testSubscriptionGivenSingleTopicWhenPublisherSendsStringPayloadAndSameTopicThenSubscriberReceivesStringPayload() throws Exception {
        String messagePayload = "Test A";
        String topic = TOPIC_STRING_A;

        try (MqttClient testPublisher = createTestMqttPublisher(topic, messagePayload)) {
            awaitMqtt().until(() -> testMqttMessageHandlerSingle.collector.isMessageReceived());

            TestMqttMessageCollector.ReceivedMessage receivedMqttMessage = testMqttMessageHandlerSingle.collector.getMessage();
            assertThat(receivedMqttMessage.getPayload()).asString().isEqualTo(messagePayload);
            assertThat(receivedMqttMessage.getQos()).isEqualTo(MqttQoS.AT_MOST_ONCE);

            testPublisher.disconnect();
        }
    }

    @Slf4j
    public static class TestMqttMessageHandlerSingle {

        public final TestMqttMessageCollector collector = new TestMqttMessageCollector();

        @MqttMapping(value = TOPIC_STRING_A)
        public void someSubscription(String topic, MqttMessage message, @MqttPayload String payload) {
            collector.addReceivedMessage(topic, message, payload);
        }
    }
}
