package com.tincore.boot.mqtt.test.support;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.mqtt.*;
import com.tincore.boot.mqtt.autoconfigure.MqttAutoConfiguration;
import io.moquette.broker.Server;
import io.moquette.broker.config.MemoryConfig;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.awaitility.Awaitility;
import org.awaitility.core.ConditionFactory;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Properties;

import static io.moquette.BrokerConstants.*;
import static java.nio.charset.StandardCharsets.UTF_8;

@Slf4j
@Import(MqttAutoConfiguration.class)
@ComponentScan(basePackageClasses = {MqttClientManager.class})
public class AbstractMqttApplicationTests extends AbstractIntegrationTest implements TestFixtureTrait {

    public static final String CLIENT_DEFAULT = "testClientDefault";
    public static final ConditionFactory AWAIT = Awaitility.await();

    @TempDir public Path tempDirectory;
    @Autowired protected MqttClientManager mqttClientManager;
    @Autowired protected MqttPublisher mqttPublisher;
    @Autowired protected MqttProperties mqttProperties;
    @Autowired protected MqttSubscriberRegistry mqttSubscriberRegistry;

    @Value("${test.mqtt.port}")
    private String mqttPort;

    private Server mqttServer;

    private String dbPath;

    public ConditionFactory awaitMqtt() {
        return AWAIT.atMost(Duration.ofSeconds(5));
    }

    public MqttCallback createMqttCallback() {
        return new MqttCallback() {
            @Override
            public void connectionLost(Throwable throwable) {
                log.info("connectionLost");
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {
                log.info("deliveryComplete");
            }

            @Override
            public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
                log.info("messageArrived");
            }
        };
    }

    @SneakyThrows
    public MqttClient createPahoMqttClient(String clientId) {
        return new MqttClient("tcp://localhost:" + mqttPort, clientId, new MemoryPersistence());
        //        return new MqttClient("tcp://localhost:" + mqttPort, clientId, new MqttDefaultFilePersistence(createDirectory(tempDirectory, clientId).getAbsolutePath()));
    }

    public MqttClient createTestMqttPublisher() throws MqttException {
        MqttClient testPublisher = createPahoMqttClient("testPublisher" + System.nanoTime());
        testPublisher.connect();
        return testPublisher;
    }

    public MqttClient createTestMqttPublisher(String topic, String sendMessagePayload) throws MqttException {
        MqttClient testPublisher = createTestMqttPublisher();
        testPublisher.publish(topic, sendMessagePayload.getBytes(UTF_8), MqttQoS.EXACTLY_ONCE.getValue(), false);
        return testPublisher;
    }

    @SneakyThrows
    @BeforeEach
    public void setUp() {
        dbPath = tempDirectory.toAbsolutePath() + File.separator + DEFAULT_MOQUETTE_STORE_H2_DB_FILENAME;
        startMqttServer(dbPath);

        setUpClientConnected();
    }

    public void setUpClientConnected() {
        mqttClientManager.connectAll(Duration.ZERO);
        awaitMqtt().until(() -> mqttClientManager.isClientConnected(CLIENT_DEFAULT));
    }

    public void setUpClientDisconnected() {
        mqttClientManager.disconnectAll();
        awaitMqtt().until(() -> !mqttClientManager.isClientConnected(CLIENT_DEFAULT));
    }

    private void startMqttServer(String dbPath) throws IOException {
        Server mqttServer = new Server();

        Properties properties = new Properties();
        properties.put(PERSISTENT_STORE_PROPERTY_NAME, dbPath);
        properties.put(PORT_PROPERTY_NAME, mqttPort);

        MemoryConfig mqttServerConfig = new MemoryConfig(properties);
        mqttServer.startServer(mqttServerConfig);

        this.mqttServer = mqttServer;
    }

    private void stopMqttServer() {
        mqttServer.stopServer();
    }

    @AfterEach
    public void tearDown() {
        setUpClientDisconnected();
        stopMqttServer();
    }
}
