package com.tincore.boot.mqtt.test.support;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.mqtt.MqttProperties;
import com.tincore.boot.mqtt.MqttQoS;
import com.tincore.boot.mqtt.MqttSubscriberMapping;
import lombok.SneakyThrows;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public interface TestFixtureTrait {

    @SneakyThrows
    default File createDirectory(Path parent, String child) {
        final Path newPath = parent.resolve(child);
        final File dir = newPath.toFile();
        if (!dir.mkdirs()) {
            throw new IOException("Can't create the new folder path: " + dir.getAbsolutePath());
        }
        return dir;
    }

    default MqttProperties createMqttProperties() {
        return MqttProperties.builder().enabled(false).build();
    }

    default MqttSubscriberMapping createMqttSubscriberMapping(String topicFilterExpression, Map<String, Class<?>> paramTypes) {
        return new MqttSubscriberMapping(topicFilterExpression, MqttQoS.AT_MOST_ONCE, false, null, paramTypes);
    }

    default MqttSubscriberMapping createMqttSubscriberMapping(String topicFilterExpression) {
        return createMqttSubscriberMapping(topicFilterExpression, Collections.emptyMap());
    }

    default List<MqttSubscriberMapping> createMqttSubscriberMappings(String... expressions) {
        return Stream.of(expressions).map(this::createMqttSubscriberMapping).collect(Collectors.toList());
    }
}
