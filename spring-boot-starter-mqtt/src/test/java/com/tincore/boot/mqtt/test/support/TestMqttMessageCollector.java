package com.tincore.boot.mqtt.test.support;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.mqtt.MqttQoS;
import lombok.Data;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class TestMqttMessageCollector implements MqttCallback {

    private BlockingQueue<ReceivedMessage> receivedMessages = new LinkedBlockingQueue<>();
    private boolean connectionLost;
    private volatile boolean messageReceived = false;

    public void addReceivedMessage(String topic, MqttMessage message, Object payload) {
        receivedMessages.offer(new ReceivedMessage(message, topic, payload));
        messageReceived = true;
    }

    @Override
    public void connectionLost(Throwable cause) {
        connectionLost = true;
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {}

    public ReceivedMessage getMessage() throws InterruptedException {
        ReceivedMessage receivedMessage = receivedMessages.take();
        messageReceived = false;
        return receivedMessage;
    }

    public boolean isConnectionLost() {
        return connectionLost;
    }

    public boolean isMessageReceived() {
        return messageReceived;
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) {
        addReceivedMessage(topic, message, message.getPayload());
    }

    void reset() {
        receivedMessages = new LinkedBlockingQueue<>();
        connectionLost = false;
        messageReceived = false;
    }

    @Data
    public static final class ReceivedMessage {

        private final MqttMessage message;
        private final String topic;
        private final Object payload;

        private ReceivedMessage(MqttMessage message, String topic, Object payload) {
            this.message = message;
            this.topic = topic;
            this.payload = payload;
        }

        public MqttQoS getQos() {
            return MqttQoS.byValue(message.getQos());
        }
    }
}
