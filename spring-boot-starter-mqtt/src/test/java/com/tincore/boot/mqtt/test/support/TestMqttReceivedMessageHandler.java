package com.tincore.boot.mqtt.test.support;

/*-
 * #%L
 * spring-boot-starter-mqtt
 * %%
 * Copyright (C) 2021 tincore
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.tincore.boot.mqtt.MqttReceivedMessageHandler;
import com.tincore.boot.mqtt.MqttSubscriberMapping;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class TestMqttReceivedMessageHandler implements MqttReceivedMessageHandler {
    private final TestMqttMessageCollector messageCollector = new TestMqttMessageCollector();

    public TestMqttMessageCollector getMessageCollector() {
        return messageCollector;
    }

    @Override
    public void onMessage(String topic, MqttMessage mqttMessage, MqttSubscriberMapping mqttSubscriberMapping, String clientId) {
        messageCollector.messageArrived(topic, mqttMessage);
    }
}
